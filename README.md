# LatestImages



## Getting started

1. Download the source code
2. Open the Terminal and write cd path_to_source_code
3. Then type "pod install" and run
4. You are ready to use the project.  

## Third Party Library Used

1. ### SDWebImage
This is used to fetch images and cache them for faster loading.

## Screenshots

![image](https://gitlab.com/PrashantAhar/latestimages/-/blob/main/Grid%20Style.png)
![image](https://gitlab.com/PrashantAhar/latestimages/-/blob/main/List%20Style.png)
