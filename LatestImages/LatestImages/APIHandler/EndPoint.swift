//
//  EndPoint.swift
//  LatestImages
//

import Foundation

enum GalleryEndpoint: EndPointType {
    case getGallery(query: String)

    var baseURL: URL { URL(string: "https://api.imgur.com/3/gallery/")! }

    static let clientId = "ec49a49a478ce37"
    static let clientSecret = "17693ba6a26d3597d21a7731bba76cfa5d76d51f"

    var path: String {
        switch self {
        case .getGallery:
            return "search/top"
        }
    }

    var httpMethod: HTTPMethod {
        switch self {
        case .getGallery:
            return .get
        }
    }

    var task: HTTPTask {
        switch self {
        case .getGallery(let query):
            let urlParams = [
                "q": query,
                "q_type": " jpg | png"
            ]
            let additionHeaders = [NetworkHeaders.authorization: "Client-ID \(Self.clientId)"]
            return .requestParametersAndHeaders(
                bodyParameters: nil,
                urlParameters: urlParams,
                additionHeaders: additionHeaders
            )
        }
    }

    var headers: HTTPHeaders? { nil }
}
