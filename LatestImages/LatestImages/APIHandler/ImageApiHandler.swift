//
//  ImageApiHandler.swift
//  LatestImages
//

import Combine
import UIKit

protocol GalleryRemoteAPI: AnyObject {
    func getGallery(for query: String) -> AnyPublisher<GalleryResult, Error>
}

final class GalleryFetcher: GalleryRemoteAPI {
    let router: APIRouter<GalleryEndpoint>
    let responseHandler: RemoteAPIResponseHandler

    init() {
        self.router = APIRouter<GalleryEndpoint>()
        self.responseHandler = DefaultRemoteAPIResponseHandler()
    }

    func getGallery(for query: String) -> AnyPublisher<GalleryResult, Error> {
        requestPublisher(.getGallery(query: query), responseHandler: responseHandler)
    }

    private func requestPublisher<T>(
        _ route: GalleryEndpoint,
        responseHandler: RemoteAPIResponseHandler?,
        decodingStrategy: JSONDecoder.KeyDecodingStrategy = .convertFromSnakeCase
    ) -> AnyPublisher<T, Error>  where T: Decodable {
        return Deferred {
            Future { promise in
                self.router.request(route, responseHandler: responseHandler, decodingStrategy: decodingStrategy) { (result: Result<T, Error>) in
                    switch result {
                    case .success(let response):
                        promise(.success(response))
                    case .failure(let error):
                        promise(.failure(error))
                    }
                }
            }
        }.retry(1)
            .eraseToAnyPublisher()
    }
}
