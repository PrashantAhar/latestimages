//
//  GalleryResponse.swift
//  LatestImages
//

import Foundation

// Gallery api response 
struct GalleryResult: Decodable {
    let data: [Gallery]
    let success: Bool
    let status: Int
}

struct Gallery: Decodable {
    let title: String
    let datetime: Double
    let images: [Image]?
    var imagesCount: Int?
    
    struct Image: Decodable {
        let link: String
        let id: String
    }
}
