//
//  GalleryViewModel.swift
//  LatestImages
//

import Foundation

// View Model for UI
struct GalleryViewModel {
    private let title: String
    private let imageLink: String
    private let imageId: String
    private let date: Double
    private let imageCount: Int?
    
    init(title: String,
         imageLink: String,
         imageId: String,
         date: Double,
         imageCount: Int?) {
        self.title = title
        self.imageLink = imageLink
        self.imageId = imageId
        self.date = date
        self.imageCount = imageCount
    }
    
    // Getters
    var displayTitle: String {
        title
    }
    var additionalImageCount: Int {
        guard let count = imageCount else { return 0 }
        return count == 0 ? 0 : count - 1
    }
    var thumbnail: URL? {
        let new = imageLink.replacingOccurrences(of: imageId, with: "\(imageId)b")
        guard let url = URL(string: new) else { return nil }
        return url
    }
    var localDate: String {
        let date = Date(timeIntervalSince1970: date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yy hh:mm a"
        dateFormatter.timeZone = .current
        return dateFormatter.string(from: date)
    }
}
