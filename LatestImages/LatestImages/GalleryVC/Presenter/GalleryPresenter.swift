//
//  GalleryPresenter.swift
//  LatestImages
//

import Foundation

// Interface for Gallery Presenter
protocol GalleryPresentable {
    var view: GalleryViewing? { get }
    
    func viewDidLoad()
    func didToggle()
    func getImageListForSearch(text: String)
}

class GalleryPresenter {
    // MARK: Properties
    weak var view: GalleryViewing?
    private let interactor: GalleryInteracting
    
    private enum Constants {
        static let network = "No response from the server. Please try again"
        static let OK = "ОK"
    }
    
    private lazy var styleDelegates: [DisplayStyle: CollectionViewDelegate] = {
        [.list: ListCollectionViewDelegate(),
         .grid: GridCollectionViewDelegate()]
    }()
    
    private lazy var selectedStyle: DisplayStyle = .list {
        didSet { updatePresentationStyle() }
    }
    
    init (view: GalleryViewing, interactor: GalleryInteracting) {
        self.view = view
        self.interactor = interactor
    }
    // changing presentation of Controller's display style
    private func updatePresentationStyle() {
        guard let delegate = styleDelegates[selectedStyle] else { return }
        let styleModel = StyleViewModel(displayStyle: selectedStyle, styleDelegate: delegate)
        view?.updatePresentationStyle(styleModel: styleModel)
    }
    
    // Converting API response to UI specific models
    private func getGalleryViewModel(for galleries: [Gallery]) -> [GalleryViewModel] {
        var result = [GalleryViewModel]()
        for gallery in galleries {
            guard let image = gallery.images?.first else { break }
            let vm = GalleryViewModel(title: gallery.title,
                                      imageLink: image.link,
                                      imageId: image.id,
                                      date: gallery.datetime,
                                      imageCount: gallery.imagesCount)
            result.append(vm)
        }
        return result
    }
}

// MARK: - Presenter taking decisions for Controller
extension GalleryPresenter: GalleryPresentable {
    func viewDidLoad() {
        updatePresentationStyle()
    }
    
    func didToggle() {
        let allCases = DisplayStyle.allCases
        guard let index = allCases.firstIndex(of: selectedStyle) else { return }
        let nextIndex = (index + 1) % allCases.count
        selectedStyle = allCases[nextIndex]
    }
    
    // Presenter informing interactor for getting data from api
    func getImageListForSearch(text: String) {
        interactor.getImageListForSearch(text: text) { [weak self] response in
            guard let response = response,
                  response.success,
                  response.status == 200,
                  !response.data.isEmpty else {
                let alert = Alert(message: Constants.network, actionTitle: Constants.OK)
                self?.view?.showAlert(alert: alert)
                return
            }
            // filtering and reverse sorting the result.
            let filteredData = response.data.filter { $0.images != nil }.sorted { $0.datetime > $1.datetime }
            self?.view?.loadGallery(data: self?.getGalleryViewModel(for: filteredData) ?? [])
        }
    }
}
