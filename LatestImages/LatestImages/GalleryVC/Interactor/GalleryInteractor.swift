//
//  GalleryInteractor.swift
//  LatestImages
//

import Foundation
import Combine

// Interface for Interactor
protocol GalleryInteracting {
    func getImageListForSearch(text: String, completion: @escaping (GalleryResult?) -> Void)
}

final class GalleryInteractor: GalleryInteracting {
    private var bag = [AnyCancellable]()
    
    // Remote API Client
    let galleryRemoteAPI: GalleryRemoteAPI
    
    init(galleryRemoteAPI: GalleryRemoteAPI) {
        self.galleryRemoteAPI = galleryRemoteAPI
    }
    
    func getImageListForSearch(text: String, completion: @escaping (GalleryResult?) -> Void) {
        galleryRemoteAPI.getGallery(for: text)
            .receive(on: DispatchQueue.main)
            .sink { result in
                switch result {
                case .failure(let error):
                    print("Response from getGallery: \(error.localizedDescription)")
                    completion(nil)
                case .finished:
                    print("finished getGallery")
                }
            } receiveValue: { response in
                completion(response)
            }
            .store(in: &bag)
    }
}
