//
//  Alert.swift
//  LatestImages
//

import Foundation

struct Alert {
    let message: String
    let actionTitle: String
}
