//
//  GalleryBuilder.swift
//  LatestImages
//

import UIKit

struct GalleryBuilder {
    static func buildGalleryVC() -> UIViewController {
        let vc = GalleryViewController()
        let galleryRemoteAPI = GalleryFetcher()
        let interactor = GalleryInteractor(galleryRemoteAPI: galleryRemoteAPI)
        let presenter = GalleryPresenter(view: vc, interactor: interactor)
        vc.presenter = presenter
        return vc
    }
}
