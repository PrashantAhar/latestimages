//
//  StyleViewModel.swift
//  LatestImages
//

import UIKit

enum DisplayStyle: String, CaseIterable {
    case list
    case grid
    
    var buttonImage: UIImage {
        switch self {
        case .list: return #imageLiteral(resourceName: "list")
        case .grid: return #imageLiteral(resourceName: "grid")
        }
    }
}

// display style view model for any controller
struct StyleViewModel {
    var displayStyle: DisplayStyle
    var styleDelegate: CollectionViewDelegate
}
