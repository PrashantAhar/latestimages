//
//  GalleryViewController.swift
//  LatestImages
//

import UIKit

// Interface for Gallery View Controller
protocol GalleryViewing: AnyObject {
    var presenter: GalleryPresentable? { get set }
    
    func showAlert(alert: Alert)
    func loadGallery(data: [GalleryViewModel])
    func updatePresentationStyle(styleModel: StyleViewModel)
}

class GalleryViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet private weak var buttonToggle: UIButton!
    @IBOutlet private weak var collection: UICollectionView!
    @IBOutlet private weak var textFieldSearch: UITextField!
    
    // MARK: Properties
    private var activityIndicator: UIActivityIndicatorView!
    var presenter: GalleryPresentable?
    private var dataSource: [GalleryViewModel]?
    
    // MARK: Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        presenter?.viewDidLoad()
    }
    
    private func setupUI() {
        activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
        textFieldSearch.delegate = self
        collection.dataSource = self
        collection.register(GalleryListCell.nib,
                            forCellWithReuseIdentifier: GalleryListCell.reuseID)
    }
    
    private func hideLoaderAndEnableToggle() {
        activityIndicator.removeFromSuperview()
        buttonToggle.isEnabled = true
    }
    
    @IBAction func didTappedChangeStyle(_ sender: Any) {
        presenter?.didToggle()
    }
}

// MARK: - Gallery Viewing (Controller will inform the events to its Presenter)
extension GalleryViewController: GalleryViewing {
    func updatePresentationStyle(styleModel: StyleViewModel) {
        collection.delegate = styleModel.styleDelegate
        buttonToggle.setImage(styleModel.displayStyle.buttonImage, for: .normal)
        collection.performBatchUpdates { collection.reloadData() }
    }
    
    func loadGallery(data: [GalleryViewModel]) {
        dataSource = data
        hideLoaderAndEnableToggle()
        collection.reloadData()
    }
    
    func showAlert(alert: Alert) {
        hideLoaderAndEnableToggle()
        let controller = UIAlertController(title: alert.message, message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: alert.actionTitle, style: .default, handler:  nil)
        controller.addAction(action)
        self.present(controller, animated: true, completion: nil)
    }
}

// MARK: - Text Field Delegate
extension GalleryViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text, !text.isEmpty else { return true }
        
        textField.addSubview(activityIndicator)
        activityIndicator.frame = textField.bounds
        activityIndicator.startAnimating()
        
        presenter?.getImageListForSearch(text: text)
        
        buttonToggle.isEnabled = false
        textField.text = nil
        textField.resignFirstResponder()
        return true
    }
}

// MARK: - Collection View Data Source
extension GalleryViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GalleryListCell.reuseID, for: indexPath) as? GalleryListCell else {
            fatalError("Something wrong with cell")
        }
        cell.gallery = dataSource?[indexPath.item]
        return cell
    }
}
