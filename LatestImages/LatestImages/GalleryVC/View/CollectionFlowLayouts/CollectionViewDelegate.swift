//
//  CollectionViewDelegate.swift
//  LatestImages
//

import UIKit

protocol CollectionViewDelegate: UICollectionViewDelegateFlowLayout {
    var sectionInsets: UIEdgeInsets { get }
    var minimumItemSpacing: CGFloat { get }
}

extension CollectionViewDelegate {
    var sectionInsets: UIEdgeInsets {
        UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
    }
    var minimumItemSpacing: CGFloat { 8 }
}
