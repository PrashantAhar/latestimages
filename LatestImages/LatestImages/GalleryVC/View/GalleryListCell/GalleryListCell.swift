//
//  GalleryListCell.swift
//  LatestImages
//

import UIKit
import SDWebImage

class GalleryListCell: UICollectionViewCell {
    static let reuseID = String(describing: GalleryListCell.self)
    static let nib = UINib(nibName: String(describing: GalleryListCell.self), bundle: nil)
    
    @IBOutlet private weak var topStack: UIStackView!
    @IBOutlet private weak var imageGallery: UIImageView!
    @IBOutlet private weak var labelDate: UILabel!
    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var labelExtraImagesCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 5
        imageGallery.layer.cornerRadius = 3
        imageGallery.contentMode = .scaleAspectFill
        imageGallery.clipsToBounds = true
        imageGallery.layer.borderWidth = 1
        imageGallery.layer.borderColor = UIColor.black.cgColor
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateContentStyle()
    }
    
    var gallery: GalleryViewModel? {
        didSet {
            guard let gallery = gallery else { return }
            imageGallery.sd_setImage(with: gallery.thumbnail)
            labelDate.text = "\(gallery.localDate)"
            labelTitle.text = gallery.displayTitle
            labelExtraImagesCount.text = "more images: \(gallery.additionalImageCount)"
        }
    }
    
    // this method will check the bounds and align the views according to the requirements
    private func updateContentStyle() {
        let isHorizontalStyle = bounds.width > bounds.height
        let oldAxis = topStack.axis
        let newAxis: NSLayoutConstraint.Axis = isHorizontalStyle ? .horizontal : .vertical
        guard oldAxis != newAxis else { return }

        topStack.axis = newAxis
        topStack.spacing = isHorizontalStyle ? 16 : 4
    }
}
